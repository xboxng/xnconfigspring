/*
 *  Copyright Qiang Yu (qiangyu #### gmail.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.xboxng.config.spring;

import com.xboxng.config.ProfileResolver;
import com.xboxng.config.XNConfig;
import com.xboxng.config.exception.XNConfigException;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.FactoryBeanNotInitializedException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.List;

public class XNConfigFactoryBean implements FactoryBean<XNConfig>, ApplicationContextAware,
        InitializingBean, BeanFactoryPostProcessor{
    private XNConfig xnconfig;
    private Object lock = new Object();

    // configured in spring
    private String configFile;
    private String profile;
    private ProfileResolver profileResolver;
    private ApplicationContext applicationContext;
    private String beanPrefix = "";

    // entries to import into spring context
    private List<String> configEntries;

    //  interface FactoryBean
    @Override
    public XNConfig getObject() throws FactoryBeanNotInitializedException {
        synchronized(lock){
            if(xnconfig==null){
                try {
                    xnconfig= new XNConfig(applicationContext.getResource(configFile).getInputStream());
                } catch (IOException e) {
                    throw new FactoryBeanNotInitializedException("cannot access XNConfig file:" + e.getMessage());
                } catch (XNConfigException e){
                    throw new FactoryBeanNotInitializedException("cannot create XNConfig bean:" + e.getMessage());
                }

                if(profileResolver==null){
                    xnconfig.setProfileResolver(new ProfileResolver() {
                        public String getProfile() {
                            return profile;
                        }
                    });
                }else{
                    xnconfig.setProfileResolver(profileResolver);
                }
            }
        }
        return xnconfig;
    }

    @Override
    public Class<?> getObjectType() {
        return XNConfig.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    //  interface InitializingBean
    @Override
    public void afterPropertiesSet() throws Exception {
        if(!StringUtils.hasLength(configFile)){
            throw new XNConfigSpringException("missing config file");
        }

        if(StringUtils.hasLength(profile) && profileResolver!=null){
            throw new XNConfigSpringException("cannot set both profile and profileResolver");
        }

        if(!StringUtils.hasLength(profile) && profileResolver==null){
            throw new XNConfigSpringException("must set either profile or profileResolver");
        }
    }

    // interface ApplicationContextAware
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        XNConfig config = this.getObject();
        if(configEntries == null || configEntries.isEmpty()){ // add all beans into spring context
            for(String entry: config.allEntries()){
                configurableListableBeanFactory.registerSingleton(beanPrefix+entry, config.getValue(entry));
            }
        }else{
            for(String entry: configEntries){
                configurableListableBeanFactory.registerSingleton(beanPrefix+entry, config.getValue(entry));
            }
        }
    }


    public String getConfigFile() {
        return configFile;
    }

    public void setConfigFile(String configFile) {
        this.configFile = configFile;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public ProfileResolver getProfileResolver() {
        return profileResolver;
    }

    public void setProfileResolver(ProfileResolver profileResolver) {
        this.profileResolver = profileResolver;
    }

    public List<String> getConfigEntries() {
        return configEntries;
    }

    public void setConfigEntries(List<String> configEntries) {
        this.configEntries = configEntries;
    }

    public String getBeanPrefix() {
        return beanPrefix;
    }

    public void setBeanPrefix(String beanPrefix) {
        this.beanPrefix = beanPrefix;
    }
}
