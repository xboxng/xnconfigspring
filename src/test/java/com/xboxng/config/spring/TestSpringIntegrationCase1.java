/*
 *  Copyright Qiang Yu (qiangyu #### gmail.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.xboxng.config.spring;

import com.xboxng.config.XNConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations = {"classpath:xnconfig_1.xml", "classpath:bean.xml"} )
public class TestSpringIntegrationCase1 {
    @Resource
    SampleBean sampleBean;

    @Test
    public void testInjectAllBeansToSpringApplicationContext(){
        assertEquals("http://www.xboxng.com", sampleBean.getUrl());
        assertEquals("hello_world", sampleBean.getPassword());
        assertEquals("http://www.xboxng.com", sampleBean.getOptions().get("website"));
        assertTrue(((List) sampleBean.getOptions().get("roles")).contains("user"));
        assertTrue(((List) sampleBean.getOptions().get("roles")).contains("guest"));
    }
}
