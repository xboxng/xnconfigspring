/*
 *  Copyright Qiang Yu (qiangyu #### gmail.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.xboxng.config.spring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations = {"classpath:xnconfig_2.xml"} )
public class TestSpringIntegrationCase2 {
    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void testInjectFewBeansToSpringApplicationContext(){
        assertEquals("http://www.xboxng.com", applicationContext.getBean("website"));
        assertEquals("hello_world", applicationContext.getBean("password"));
    }

    @Test(expected = NoSuchBeanDefinitionException.class)
    public void testNoInjectBeansToSpringApplicationContext(){
        assertEquals("http://www.xboxng.com", applicationContext.getBean("options"));
    }
}
